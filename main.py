from fastapi import FastAPI,Query,Depends
from typing import Optional,List
from pydantic import BaseModel
from database import engine,Sessionlocal,Base
from sqlalchemy import String,Integer,Column
from sqlalchemy.orm import Session
from fastapi.responses import JSONResponse

app = FastAPI()

class User(Base):
    __tablename__="user"
    id = Column(Integer,primary_key = True,index = True)
    name = Column(String(20),index=True)
    email = Column(String(20),index=True)
    password = Column(String(20))

class UserSchema(BaseModel):
    name:str
    email:str

    class Config:
        orm_mode = True

class UserCreateSchema(UserSchema):
    password:str

def get_db():
    db=Sessionlocal()
    try:
        yield db
    finally:
        db.close()

Base.metadata.create_all(bind=engine)

@app.post("/users",response_model=UserSchema)
def post(user:UserCreateSchema,db:Session=Depends(get_db)):
    u = User(name=user.name,email =user.email,password=user.password)
    db.add(u)
    db.commit()
    return u

@app.get("/users",response_model=list[UserSchema])
def get(db:Session=Depends(get_db)):
    return db.query(User).all()

@app.put("/users/{user_id}",response_model=UserSchema)
def put(user_id:int,user:UserCreateSchema,db:Session=Depends(get_db)):
    u=db.query(User).filter(user_id==User.id).first()
    u.name = user.name
    u.email = user.email
    u.password = user.password
    db.add(u)
    db.commit()
    return u

@app.delete("/users/{user_id}")
def delete(user_id:int,db:Session=Depends(get_db)):
    u=db.query(User).filter(user_id==User.id).first()
    db.delete(u)
    return JSONResponse({'msg':'data deleted'})

