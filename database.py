from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

DB_SQLALCHEMY_URL = "mysql+pymysql://root:root@localhost/drop"

engine = create_engine(DB_SQLALCHEMY_URL)
Sessionlocal = sessionmaker(autocommit=False,bind=engine)
Base = declarative_base()